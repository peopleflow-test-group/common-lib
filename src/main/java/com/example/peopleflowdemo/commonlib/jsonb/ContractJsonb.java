package com.example.peopleflowdemo.commonlib.jsonb;


import lombok.*;

import javax.validation.constraints.*;
import java.io.Serializable;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ContractJsonb implements Serializable {

    @Min(value = 1, message = "{contract.termMonth.min} {value} month(s)")
    @Max(value = 60, message = "{contract.termMonth.max} {value} month(s)")
    @NotNull
    private Integer termMonths;

    @Size(min = 10, max = 300)
    @NotBlank
    @NotNull
    private String responsibilities;

    @Size(min = 10, max = 300)
    @NotBlank
    @NotNull
    private String agreements;

}
