package com.example.peopleflowdemo.commonlib.kafka.dto;

import lombok.*;

import java.time.ZonedDateTime;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResultKafkaDto {

    private Long id;
    private ZonedDateTime openDate;
    private String responseCode;


    public static ResultKafkaDto createSuccess(Long id, ZonedDateTime openDate) {
        return new ResultKafkaDto(id, openDate, null);
    }


    public static ResultKafkaDto createExceptional(String responseCode) {
        return new ResultKafkaDto(null, null, responseCode);
    }
}
