package com.example.peopleflowdemo.commonlib.kafka.dto;

import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChangeEmployeeStateKafkaDto {

    private Long employeeId;
    private StateEvent event;

}
