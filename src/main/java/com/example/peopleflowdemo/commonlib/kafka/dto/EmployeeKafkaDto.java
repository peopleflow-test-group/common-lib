package com.example.peopleflowdemo.commonlib.kafka.dto;

import com.example.peopleflowdemo.commonlib.jsonb.ContractJsonb;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeKafkaDto {

    private String pssReadyToPersist;

    private String firstName;

    private String lastName;

    private int age;

    private String mobilePhone;

    private String email;

    private EmployeeStateEnum state;

    private ContractJsonb contract;
}
