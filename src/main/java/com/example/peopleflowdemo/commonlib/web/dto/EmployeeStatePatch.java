package com.example.peopleflowdemo.commonlib.web.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Schema(description = "employee state PATCH request body")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeStatePatch {

    @NotNull
    @Pattern(regexp = "CHECK|APPROVE|ACTIVATE", message = "{employeeStatePatch.regexp}: {regexp}")
    @Schema(description = "an event for changing employee state")
    private String event;
}
