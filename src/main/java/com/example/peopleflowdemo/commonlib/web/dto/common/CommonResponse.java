package com.example.peopleflowdemo.commonlib.web.dto.common;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Schema(description = "4xx/5xx response body")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CommonResponse {

    @Schema(description = "internal code")
    private String code;

    @Schema(description = "message for developer")
    private String devMessage;

    @Schema(description = "multilingual user message for customer")
    private String userMessage;


}
