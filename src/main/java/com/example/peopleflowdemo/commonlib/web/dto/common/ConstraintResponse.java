package com.example.peopleflowdemo.commonlib.web.dto.common;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Schema(description = "detailed validation response body")
@AllArgsConstructor
@Data
public class ConstraintResponse {

    @Schema(description = "validation messages per attribute")
    private Map<String, String> validationMessage;

}
