package com.example.peopleflowdemo.commonlib.web.dto;

import com.example.peopleflowdemo.commonlib.jsonb.ContractJsonb;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;

@Schema(description = "employee POST request body")
@Data
public class EmployeePost {

    @Schema(description = "password")
    @NotNull
    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*_]{8,20}")
    private String password;

    @Schema(description = "first name")
    @NotEmpty
    @NotNull
    @Pattern(regexp = "^[\\p{L}\\s.’\\-,]+$")
    private String firstName;

    @Schema(description = "last name")
    @NotEmpty
    @NotNull
    @Pattern(regexp = "^[\\p{L}\\s.’\\-,]+$")
    private String lastName;

    @Schema(description = "age")
    @NotNull
    @Min(value = 18, message = "{employeePost.age.min} {value}")
    @Max(value = 150, message = "{employeePost.age.max} {value}")
    private int age;

    @Schema(description = "mobile phone")
    @NotNull
    @Pattern(regexp = "\\d{12}")
    private String mobilePhone;

    @Schema(description = "email address")
    @NotEmpty
    @NotNull
    @Email
    private String email;

    @Schema(description = "employee contract details")
    @Valid
    @NotNull
    private ContractJsonb contract;

}
