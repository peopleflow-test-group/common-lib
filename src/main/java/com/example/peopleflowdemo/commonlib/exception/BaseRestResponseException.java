package com.example.peopleflowdemo.commonlib.exception;


import com.example.peopleflowdemo.commonlib.web.dto.common.CommonResponse;
import lombok.Getter;

/**
 * Is thrown with custom response body in exceptional cases
 */
@Getter
public class BaseRestResponseException extends RuntimeException {

    private int httpStatus;

    private CommonResponse responseBody;


    public BaseRestResponseException(int httpStatus, String code, String exceptionMessage, String devMessage, String userMessage) {
        super(exceptionMessage);
        this.httpStatus = httpStatus;
        this.responseBody = new CommonResponse(
                code,
                devMessage,
                userMessage);
    }
}
