package com.example.peopleflowdemo.commonlib.repository;


import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * employee read/write jpa repository
 */
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {


    /**
     * performs select to find an employee by phone
     *
     * @param mobilePhone where param
     * @return employee entity if exists
     */
    Optional<EmployeeEntity> findByMobilePhone(String mobilePhone);


    /**
     * returns ordered employee list
     *
     * @return employee entity list
     */
    List<EmployeeEntity> findAllByOrderByEmployeeIdAsc();

    /**
     * native employee state update
     *
     * @param state      new state
     * @param employeeId target employee id
     */
    @Modifying
    @Query(value = "update employees set state =:state where employee_id =:employeeId", nativeQuery = true)
    void nativeStateUpdate(String state, Long employeeId);


}
