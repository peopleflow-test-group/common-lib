package com.example.peopleflowdemo.commonlib.repository.entity;

import com.example.peopleflowdemo.commonlib.jsonb.ContractJsonb;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@DynamicUpdate
@Table(name = "employees")
@Entity
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "open_date")
    private ZonedDateTime openDate;

    @Column(name = "close_date")
    private ZonedDateTime closeDate;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "age")
    private int age;

    @Column(name = "mobile_phone")
    private String mobilePhone;

    @Column(name = "email")
    private String email;

    @Enumerated(EnumType.STRING)
    private EmployeeStateEnum state;

    @Type(type = "jsonb")
    @Column(name = "contract", columnDefinition = "jsonb")
    private ContractJsonb contract;


    public EmployeeEntity(ZonedDateTime openDate, ZonedDateTime closeDate, String password, String firstName, String lastName, int age, String mobilePhone, String email, EmployeeStateEnum state, ContractJsonb contract) {
        this.openDate = openDate;
        this.closeDate = closeDate;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.mobilePhone = mobilePhone;
        this.email = email;
        this.state = state;
        this.contract = contract;
    }
}
