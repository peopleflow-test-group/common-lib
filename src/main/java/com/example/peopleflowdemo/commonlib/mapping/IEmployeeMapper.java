package com.example.peopleflowdemo.commonlib.mapping;

import com.example.peopleflowdemo.commonlib.kafka.dto.EmployeeKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.ResultKafkaDto;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.service.helper.EmployeeMappingHelper;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeePost;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeResponse;

import java.util.List;

/**
 * contains only simple different app layers pojo mapping logic
 */
public interface IEmployeeMapper {

    /**
     * creates an employee is ready to be stored in db
     *
     * @param kafkaDto received kafka message body
     * @param helper   includes additional params
     * @return entity is ready to be stored
     */
    EmployeeEntity toEntity(EmployeeKafkaDto kafkaDto, EmployeeMappingHelper helper);


    /**
     * transforms request body to kafka message body
     *
     * @param body   request body
     * @param helper additional attrs
     * @return kafka message body
     */
    EmployeeKafkaDto toKafkaDto(EmployeePost body, EmployeeMappingHelper helper);


    /**
     * converts entity into response body for client side
     *
     * @param entity entity
     * @return response DTO
     */
    EmployeeResponse toResponse(EmployeeEntity entity);


    /**
     * creates rest response body after replying kafka sending
     *
     * @param kafkaDto       message is going to be sent into kafka
     * @param resultKafkaDto replying from consumer message body
     * @return response body for client side with generated employee id
     */
    EmployeeResponse toResponse(EmployeeKafkaDto kafkaDto, ResultKafkaDto resultKafkaDto);


    /**
     * converts list of entity into response dto list
     *
     * @param entityList employee entity list
     * @return list of employee response bodies
     */
    List<EmployeeResponse> toResponseList(List<EmployeeEntity> entityList);
}
