package com.example.peopleflowdemo.commonlib.mapping;

import com.example.peopleflowdemo.commonlib.kafka.dto.EmployeeKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.ResultKafkaDto;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.service.helper.EmployeeMappingHelper;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeePost;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeResponse;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * contains only simple different app layers pojo mapping logic
 */
@Component
public class EmployeeMapper implements IEmployeeMapper {


    @Override
    public EmployeeEntity toEntity(EmployeeKafkaDto kafkaDto, EmployeeMappingHelper helper) {
        return new EmployeeEntity(
                helper.getOpenDate(),
                null,
                kafkaDto.getPssReadyToPersist(),
                kafkaDto.getFirstName(),
                kafkaDto.getLastName(),
                kafkaDto.getAge(),
                kafkaDto.getMobilePhone(),
                kafkaDto.getEmail(),
                kafkaDto.getState(),
                kafkaDto.getContract());
    }


    @Override
    public EmployeeKafkaDto toKafkaDto(EmployeePost body, EmployeeMappingHelper helper) {
        return new EmployeeKafkaDto(
                helper.getPssReadyToPersist(),
                body.getFirstName(),
                body.getLastName(),
                body.getAge(),
                body.getMobilePhone(),
                body.getEmail(),
                helper.getState(),
                body.getContract());
    }


    @Override
    public EmployeeResponse toResponse(EmployeeEntity entity) {
        return new EmployeeResponse(
                entity.getEmployeeId(),
                entity.getOpenDate(),
                entity.getCloseDate(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getAge(),
                entity.getMobilePhone(),
                entity.getEmail(),
                entity.getState(),
                entity.getContract());
    }


    @Override
    public EmployeeResponse toResponse(EmployeeKafkaDto kafkaDto, ResultKafkaDto resultKafkaDto) {
        return new EmployeeResponse(
                resultKafkaDto.getId(),
                resultKafkaDto.getOpenDate(),
                null,
                kafkaDto.getFirstName(),
                kafkaDto.getLastName(),
                kafkaDto.getAge(),
                kafkaDto.getMobilePhone(),
                kafkaDto.getEmail(),
                kafkaDto.getState(),
                kafkaDto.getContract());
    }


    @Override
    public List<EmployeeResponse> toResponseList(List<EmployeeEntity> entityList) {
        return entityList.stream().map(this::toResponse).toList();
    }

}
