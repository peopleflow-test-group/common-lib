package com.example.peopleflowdemo.commonlib.statemachine.state;

public enum EmployeeStateEnum {

    ADDED, IN_CHECK, APPROVED, ACTIVE

}
