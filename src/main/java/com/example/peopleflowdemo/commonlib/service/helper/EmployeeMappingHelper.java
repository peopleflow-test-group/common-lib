package com.example.peopleflowdemo.commonlib.service.helper;

import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import lombok.Getter;

import java.time.ZonedDateTime;

/**
 * pojo contains business logic necessary attributes
 */
@Getter
public class EmployeeMappingHelper {

    private String pssReadyToPersist;
    private ZonedDateTime openDate;
    private EmployeeStateEnum state;


    public static EmployeeMappingHelper createKafkaHelper(String pssReadyToPersist, EmployeeStateEnum state) {
        EmployeeMappingHelper helper = new EmployeeMappingHelper();
        helper.pssReadyToPersist = pssReadyToPersist;
        helper.state = state;
        return helper;
    }


    public static EmployeeMappingHelper createEntityHelper(ZonedDateTime openDate) {
        EmployeeMappingHelper helper = new EmployeeMappingHelper();
        helper.openDate = openDate;
        return helper;
    }
}
