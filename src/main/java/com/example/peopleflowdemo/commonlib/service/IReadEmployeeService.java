package com.example.peopleflowdemo.commonlib.service;

import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;

import java.util.List;
import java.util.function.Supplier;

/**
 * auxiliary service with employee base read business logic
 */
public interface IReadEmployeeService {


    /**
     * get or else throw runtime exception
     *
     * @param employeeId target entity id
     * @return target entity on success
     */
    EmployeeEntity getByIdOrElseThrow(Long employeeId, Supplier<RuntimeException> throwEx);


    /**
     * finds by mobile number of else throws an exception
     *
     * @param mobilePhone search criteria
     * @param ex          is thrown if employee not found
     */
    void getByMobilePhoneThenThrow(String mobilePhone, RuntimeException ex);


    /**
     * finds by mobile number of else throws an exception
     *
     * @param mobilePhone search criteria
     * @return object representation of employee
     */
    EmployeeEntity getByMobilePhoneOrElseThrow(String mobilePhone, Supplier<RuntimeException> throwEx);


    /**
     * returns full employee list
     *
     * @return entity list
     */
    List<EmployeeEntity> getAllEmployees();
}
