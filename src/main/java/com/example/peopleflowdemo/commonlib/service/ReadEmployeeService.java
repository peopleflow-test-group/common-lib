package com.example.peopleflowdemo.commonlib.service;

import com.example.peopleflowdemo.commonlib.repository.EmployeeRepository;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Supplier;

@Service
public class ReadEmployeeService implements IReadEmployeeService {


    protected final EmployeeRepository repository;

    @Autowired
    public ReadEmployeeService(EmployeeRepository repository) {
        this.repository = repository;
    }


    @Override
    public EmployeeEntity getByIdOrElseThrow(Long employeeId, Supplier<RuntimeException> throwEx) {
        return repository.findById(employeeId).orElseThrow(throwEx);
    }


    @Override
    public void getByMobilePhoneThenThrow(String mobilePhone, RuntimeException ex) {
        repository.findByMobilePhone(mobilePhone).ifPresent(entity -> {
            throw ex;
        });
    }


    @Override
    public EmployeeEntity getByMobilePhoneOrElseThrow(String mobilePhone, Supplier<RuntimeException> throwEx) {
        return repository.findByMobilePhone(mobilePhone).orElseThrow(throwEx);
    }


    @Override
    public List<EmployeeEntity> getAllEmployees() {
        return repository.findAllByOrderByEmployeeIdAsc();
    }


}